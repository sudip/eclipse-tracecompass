import java.util.*;
import java.util.jar.*;
import java.io.*;
import java.lang.*;

public class create_info {
public static void main(String[] args)throws Exception{
	if (args.length != 1) {
		System.out.println("missing argument");
		System.exit(-1);
	}
	String[] arglist = args[0].split(",");
	if (arglist.length != 3) {
		System.out.println("argument not correct");
		System.exit(-1);
	}

	Manifest m = new JarFile(arglist[0]).getManifest();
	Attributes a = m.getMainAttributes();
	String[] symname = a.getValue("Bundle-SymbolicName").split(";");
	System.out.println(symname[0] + "," + a.getValue("Bundle-Version") + "," + arglist[0] + "," + arglist[1] + "," + arglist[2]);
}
}
